<?php

namespace Tivents\VerbandonlinePhpSdk;
use GuzzleHttp\Client;

class BaseController
{

    private function setToken()
    {
        $user = $_ENV['VO_USER'];   // der API-Benutzername mit entsprechenden Rollenrechten ("admin" ist nicht zulässig!)
        $password = $_ENV['VO_PASSWORD'];   // das zugehörige Passwort
        return "A/$user/".md5($password);
    }

    public function doRequest($function, $data)
    {

        $url = 'https://www.vereinonline.org/'.$_ENV['VO_ASSOCIATION'].'/';

        $guzzleClient = new Client([
            // Base URI is used with relative requests
            'base_uri' => $url,
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]);
        $response = $guzzleClient->post($url."?api=$function", [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $this->setToken()
            ],
            'body' => json_encode($data)
        ])->getBody()->getContents();

        return $response;
    }

}