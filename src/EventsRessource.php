<?php

namespace Tivents\VerbandonlinePhpSdk;

class EventsRessource extends BaseController
{

    public function getEvents()
    {
        
    }

    public function getRegistrations($eventId, $object)
    {
        $function = 'CreateRegistration';
        $registrationData['veranstaltungid'] = $eventId;

        if($object) {
            return json_decode($this->doRequest($function, $registrationData));
        }
        else {
            return json_decode($this->doRequest($function, $registrationData), true);
        }

    }

    public function createRegistration($eventId, $registrationData = [], $object = false)
    {
        $function = 'CreateRegistration';
        $registrationData['veranstaltungid'] = $eventId;

        if($object) {
            return json_decode($this->doRequest($function, $registrationData));
        }
        else {
            return json_decode($this->doRequest($function, $registrationData), true);
        }


    }
}