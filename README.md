# Verbandonline PHP SDK

A small SDK to interact with the API Service for verband- and vereinonline

## Status
This package is in heavily development modus. Do not use it production envoriments

### ToDo
- [ ] MemberRessource
  - [ ] List members
  - [ ] Get member
  - [ ] Create member
  - [ ] Update member
  - [ ] Delete member
- [ ] EventRessource
  - [ ] List events
  - [ ] Get events
  - [ ] Create event
  - [ ] Update event
    - [ ] Get registrations
    - [ ] Create registration
    - [ ] Update registration
- [ ] NewsletterRessource
  - [ ] List newsletter
  - [ ] Get newsletter article
  - [ ] Create subscriber

## Getting started

```composer
compose require tivents/verbandonline
```

Provide in your env file your credentials
```dotenv
VO_USER=yourmightyuser
VO_PASSWORD=
VO_ASSOCIATION=
```